#pragma once
#include <Arduino.h>

//#define IOT_CONFIG_USE_X509_CERT

#define IOT_ENDPOINT "******.azure-devices.net" // Azure IoT Hubで登録したホスト名にしてください
#define IOT_PORT 8883

#ifdef IOT_CONFIG_USE_X509_CERT
#define DEVICE_NAME "********"　//Azureで登録したデバイスIDを設定してください

static const char AZURE_PRIVATE_KEY[] = 
R"(-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCWqjKkX0JI2avU
//Azureで登録した時に作られるキーを設定してください
GQx1sieREEJcFTNPIwMtFf/oEJ81htNdIXm1bwxKcuUDaAMMEpR0TZNX5lTtYPef
L7petng1YmWHxCySEJx2fpC7
-----END PRIVATE KEY-----)";

static const char AZURE_CERT_CRT[] =
R"(-----BEGIN CERTIFICATE-----
MIIDxjCCAa4CFFVllY92fTXcGlSx6z8zqEiHkrbkMA0GCSqGSIb3DQEBCwUAMCox
//Azureで登録した時に作られるキーを設定してください
4lk7Mo/Efjwv5Q==
-----END CERTIFICATE-----)";

#else
#define DEVICE_NAME "******" //Azure IoT Hubで登録したデバイスIDを設定してください
#define IOT_CONFIG_DEVICE_KEY "*****************" //Azure IoT Hubで登録した時に作られる主キー又はセカンダリーキーを設定してください
#endif

//https://www.digicert.com/kb/digicert-root-certificates.htm
//DigiCertGlobalRootG2.crt.pem
//Valid until: 15/Jan/2038
static const char AZURE_ROOT_CA[] =
R"(-----BEGIN CERTIFICATE-----
MIIDjjCCAnagAwIBAgIQAzrx5qcRqaC7KGSxHQn65TANBgkqhkiG9w0BAQsFADBh
MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBH
MjAeFw0xMzA4MDExMjAwMDBaFw0zODAxMTUxMjAwMDBaMGExCzAJBgNVBAYTAlVT
MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j
b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IEcyMIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuzfNNNx7a8myaJCtSnX/RrohCgiN9RlUyfuI
2/Ou8jqJkTx65qsGGmvPrC3oXgkkRLpimn7Wo6h+4FR1IAWsULecYxpsMNzaHxmx
1x7e/dfgy5SDN67sH0NO3Xss0r0upS/kqbitOtSZpLYl6ZtrAGCSYP9PIUkY92eQ
q2EGnI/yuum06ZIya7XzV+hdG82MHauVBJVJ8zUtluNJbd134/tJS7SsVQepj5Wz
tCO7TG1F8PapspUwtP1MVYwnSlcUfIKdzXOS0xZKBgyMUNGPHgm+F6HmIcr9g+UQ
vIOlCsRnKPZzFBQ9RnbDhxSJITRNrw9FDKZJobq7nMWxM4MphQIDAQABo0IwQDAP
BgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAdBgNVHQ4EFgQUTiJUIBiV
5uNu5g/6+rkS7QYXjzkwDQYJKoZIhvcNAQELBQADggEBAGBnKJRvDkhj6zHd6mcY
1Yl9PMWLSn/pvtsrF9+wX3N3KjITOYFnQoQj8kVnNeyIv/iPsGEMNKSuIEyExtv4
NeF22d+mQrvHRAiGfzZ0JFrabA0UWTW98kndth/Jsw1HKj2ZL7tcu7XUIOGZX1NG
Fdtom/DzMNU+MeKNhJ7jitralj41E6Vf8PlwUHBHQRFXGU7Aj64GxJUTFy8bJZ91
8rGOmaFvE7FBcf6IKshPECBV1/MUReXgRPTqh5Uykw7+U0b6LJ3/iyK5S9kJRaTe
pLiaWN0bfVKfjllDiIGknibVb63dDcY3fe0Dkhvld1927jyNxF1WW6LZZm6zNTfl
MrY=
-----END CERTIFICATE-----)";