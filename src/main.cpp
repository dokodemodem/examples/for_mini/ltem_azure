/*
 * Sample program for DokodeModem
 * MQTT pub/sub sample for Azure IoT Hub with MQTT library.
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
#include <nRF9160Modem.h>
#include <nRF9160ModemClient.h>
#include <MQTT.h>
#include <time.h>
#include <UrlEncode.h>
#include <Base64.h>
#include <ArduinoJson.h>
#include "modem_setting.h"
#include "iot_config.h"
#include "sha256.h"

#define SEND_CYCLE_MS 10000

DOKODEMO Dm = DOKODEMO();
nRF9160Modem modem = nRF9160Modem();
nRF9160ModemClient modemLTE_M(&modem);
MQTTClient mqttClient(256, 256);

uint8_t _sendbuff[4096];
const String pubTopic = "devices/" + String(DEVICE_NAME) + "/messages/events/";
const String subPath = "devices/" + String(DEVICE_NAME) + "/messages/devicebound/#";

uint32_t getUnixTime()
{
  DateTime now = Dm.rtcNow();
  std::tm res = {};
  res.tm_year = now.year();
  res.tm_mon = now.month() - 1;
  res.tm_mday = now.day();
  res.tm_hour = now.hour();
  res.tm_min = now.minute();
  res.tm_sec = now.second();

  res.tm_year -= 1900;                         // since from 1900
  uint32_t unixtime = mktime(&res) - 9 * 3600; // JST->GMT

  return unixtime;
}

uint32_t _unixtime = 0;

static bool connectMqtt()
{
  SerialDebug.println("### Connecting to MQTT server: " + String(IOT_ENDPOINT));

#ifndef IOT_CONFIG_USE_X509_CERT
  // login時に使用するusernameとpasswordを作ります
  // Azure側の仕様変更があるかもしれませんが、その際は適宜変更ください

  String username = IOT_ENDPOINT;
  username += "/";
  username += DEVICE_NAME;
  username += "/?api-version=2021-04-12";

  SerialDebug.println("username: " + username);

  _unixtime = getUnixTime() + 60; // 60分で使えなくなります
  SerialDebug.println("unixtime: " + String(_unixtime));

  String resourceURI = IOT_ENDPOINT;
  resourceURI += "/devices/";
  resourceURI += DEVICE_NAME;
  String encodedURL = urlEncode(resourceURI);

  String stringToSign = encodedURL;
  stringToSign += "\n";
  stringToSign += _unixtime;

  char key[] = IOT_CONFIG_DEVICE_KEY;
  int keylen = strlen(key);
  int decodedLength = Base64.decodedLength(key, keylen);
  char decodedKey[decodedLength + 1];
  Base64.decode(decodedKey, key, keylen);

  Sha256 sha;
  sha.initHmac((uint8_t *)decodedKey, decodedLength);
  sha.print(stringToSign);
  uint8_t hmacResult[32];
  memcpy(hmacResult, sha.resultHmac(), 32);

  char encodedString[45];
  Base64.encode(encodedString, (char *)hmacResult, 32);

  String password = "SharedAccessSignature sr=";
  password += encodedURL;
  password += "&sig=";
  password += urlEncode(encodedString);
  password += "&se=";
  password += _unixtime;

  SerialDebug.println("password: " + password);

  String device = DEVICE_NAME;

  if (!mqttClient.connect(device.c_str(), username.c_str(), password.c_str()))
  {
    SerialDebug.println("### MQTT Failed connection...");
    return false;
  }

#else
  String username = "dokodemodem.azure-devices.net/deviceX509/?api-version=2020-09-30";
  SerialDebug.println("username: " + username);

  SerialDebug.println(username);
  if (!mqttClient.connect(DEVICE_NAME, username.c_str(), nullptr))
  {
    SerialDebug.println("### MQTT Failed connection...");
    return false;
  }
#endif
  SerialDebug.print("### MQTT connected ");

  if (!mqttClient.subscribe(subPath))
  {
    SerialDebug.println("subscribe failed...");
    return false;
  }

  SerialDebug.println("subscribe: " + String(subPath));

  return true;
}

bool modem_init(bool enableSSL)
{
  SerialDebug.println("\r\n### initialize LTE-M");

  Dm.ModemPowerCtrl(ON); // モデムの電源を入れます

  if (!modem.init(UartModem)) // その直後に初期化してください
  {
    SerialDebug.println("### init Error");
    return false;
  }

  // Azure IoT hubに登録した時の各種暗号鍵を使用します
  if (enableSSL)
  {
    bool ret = modem.setCACert(AZURE_ROOT_CA);
    if (!ret)
      SerialDebug.println("error AZURE_CA");

#ifdef IOT_CONFIG_USE_X509_CERT
    ret = modem.setCertificate(AZURE_CERT_CRT);
    if (!ret)
      SerialDebug.println("error AZURE_CERT_CRT");

    bool ret = modem.setPrivateKey(AZURE_PRIVATE_KEY);
    if (!ret)
      SerialDebug.println("error AZURE_PRIVATE_KEY");
#endif
  }

  // 携帯網につなげます
  SerialDebug.println("### Start activate...");
  if (!modem.activate(APN, USERNAME, PASSWORD))
  {
    SerialDebug.println("### Error");
    return false;
  }
  SerialDebug.println("### OK");

  // 機器のIPアドレスを表示します
  std::string ipadd;
  modem.getIPaddress(&ipadd);
  SerialDebug.print("IPaddress: ");
  SerialDebug.println(ipadd.c_str());

  // 機器のUUIDアドレスを表示します
  std::string add;
  modem.getUUID(&add);
  SerialDebug.print("UUID: ");
  SerialDebug.println(add.c_str());

  // 機器のファームウェアバージョンを表示します
  modem.getVersion(&add);
  SerialDebug.print("Version: ");
  SerialDebug.println(add.c_str());

  // LTE-Mの時間をRTCに設定します
  std::tm res = {};
  if (modem.getlocaltime(&res))
  {
    DateTime tm = DateTime(res.tm_year, res.tm_mon + 1, res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec);
    Dm.rtcAdjust(tm);
  }

  return true;
}

void messageReceived(String &topic, String &payload)
{
  Dm.LedCtrl(GREEN_LED, ON);

  SerialDebug.println("incoming: " + topic + " - " + payload);

  Dm.LedCtrl(GREEN_LED, OFF);
}

void setup()
{
  delay(2000); // デバッグ出力のため、USBが認識するまで待ちます。

  Dm.begin(); // 初期化が必要です。

  SerialDebug.begin(115200); // デバッグ用
  UartModem.begin(115200);   // モデム用

  // 初期化が失敗した時のためリトライします
  int retry = 3;
  while (retry > 0)
  {
    if (modem_init(true))
      break;

    retry--;

    Dm.ModemPowerCtrl(OFF); // 電源切ってリトライします
    delay(100);
  }
  if (retry == 0)
  {
    SerialDebug.println("### MQTT Failed connection...");
    delay(100); // デバッグメッセージが出力終わるまで待ちます。
    Dm.reset(); // 再起動します
  }

  // MQTT brokerに接続します
  mqttClient.begin(IOT_ENDPOINT, IOT_PORT, modemLTE_M);
  mqttClient.onMessage(messageReceived);
  mqttClient.setOptions(30, false, 10000);

  retry = 3;
  while (retry)
  {
    bool ret = connectMqtt();
    if (ret)
      break;

    retry--;
    delay(1000);
  }
  if (!retry)
    Dm.reset();

  Dm.LedCtrl(GREEN_LED, OFF);
  Dm.LedCtrl(RED_LED, OFF);
}

uint32_t _elaspTime = 0;
uint32_t _sendCount = 1;
u_int8_t _retyrCount = 3;

void publish()
{
  JsonDocument doc;

  doc["counter"] = _sendCount++;
  doc["gpio"] = String(Dm.readExIN());
  doc["adc"] = String(Dm.readExADC(), 3);

  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer);

  if (mqttClient.publish(pubTopic, jsonBuffer, true, 1))
  {
    SerialDebug.println("published: " + String(jsonBuffer));
  }
  else
  {
    SerialDebug.println("publish failed...");
  }
}

void loop()
{
  mqttClient.loop();

  if (!mqttClient.connected())
  {
    // コネクションが切れたので再接続します
    // _unixtime秒すぎるとサーバー側から通信が切れます
    SerialDebug.println("### disconnected");

    if (!connectMqtt())
    {
      // 繋がらない場合はハードを再起動してみます
      Dm.ModemPowerCtrl(OFF);
      delay(100);
      if (modem_init(true))
      {
        if (!connectMqtt())
        {
          _retyrCount--;
        }
      }
      else
      {
        _retyrCount--;
      }
    }
    else
    {
      _retyrCount = 3;
    }

    if (_retyrCount == 0)
      Dm.reset(); // ダメそうならリセットします
  }

  // 周期的に送信します
  uint32_t timeStamp = millis();
  if ((timeStamp - _elaspTime) >= SEND_CYCLE_MS)
  {
    _elaspTime = timeStamp;
    Dm.LedCtrl(RED_LED, ON);
    publish();
    Dm.LedCtrl(RED_LED, OFF);
  }
}
